விதிகள்
=======================================

=================
மெய்மயக்கம்
=================

.. automodule:: tamilrulepy.meymayakkam
	:members:
=================
சொல் துவக்கம்
=================
.. automodule:: tamilrulepy.word_starting
        :members:
=================
சொல் இறுதி
=================
.. automodule:: tamilrulepy.word_ending
        :members: