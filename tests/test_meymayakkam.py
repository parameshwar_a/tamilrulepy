import unittest
from tamilrulepy.meymayakkam import meymayakkam_1, meymayakkam_2, meymayakkam_3, meymayakkam_4


class TestMeymayakkam(unittest.TestCase):
    
    def test_meymayakkam_1(self):
        # correct words
        self.assertTrue(meymayakkam_1("மயக்கம்"))
        self.assertTrue(meymayakkam_1("மெய்மயக்கம்"))
        # Detecting incorrect words
        self.assertFalse(meymayakkam_1("மயக்க்கம்"))
        # Detecting Not Applicable words
        self.assertIsNone(meymayakkam_1("தடம்"))
        self.assertIsNone(meymayakkam_1("திட்டு"))

    def test_meymayakkam_2(self):
        # Correct Words
        self.assertTrue(meymayakkam_2("வெங்காயம்"))
        self.assertTrue(meymayakkam_2("வேங்கை"))
        self.assertTrue(meymayakkam_2("குரங்கு"))
        self.assertTrue(meymayakkam_2("அங்குலம்"))
        # Detecting Incorrect words
        self.assertFalse(meymayakkam_2("வெங்ங்கயம்"))
        self.assertFalse(meymayakkam_2("இனிங்சி"))
        # Detecting Not Applicable words
        self.assertIsNone(meymayakkam_2("விளையாட்டு"))
        self.assertIsNone(meymayakkam_2("மீன்கள்"))
        self.assertIsNone(meymayakkam_2("சாப்பாடு"))

    def test_meymayakkam_3(self):
        # Correct Words
        self.assertTrue(meymayakkam_3("அச்சரம்"))
        self.assertTrue(meymayakkam_3("அச்சாணி"))
        self.assertTrue(meymayakkam_3("சிகிச்சை"))
        self.assertTrue(meymayakkam_3("சர்ச்சை"))
        # Detecting Incorrect words
        self.assertFalse(meymayakkam_3("சட்ச்ச்சி"))
        self.assertFalse(meymayakkam_3("திச்டட"))
        # Detecting Not Applicable words
        self.assertIsNone(meymayakkam_3("பூங்கா"))
        self.assertIsNone(meymayakkam_3("நேரம்"))
        self.assertIsNone(meymayakkam_3("நாட்கள்"))


    def test_meymayakkam_4(self):
        # Correct Words
        # TODO Still for "ஞ்","ய்" didn't have any words
        self.assertTrue(meymayakkam_4("இஞ்சி"))
        self.assertTrue(meymayakkam_4("மஞ்சள்"))
        self.assertTrue(meymayakkam_4("காஞ்சிபுரம்"))
        self.assertTrue(meymayakkam_4("அஞ்சறைப்பெட்டி"))
        # Detecting Incorrect words
        self.assertFalse(meymayakkam_4("அஞ்ககி"))
        self.assertFalse(meymayakkam_4("மிஞ்வலி"))
        # Detecting Not Applicable words
        self.assertIsNone(meymayakkam_4("காலை"))
        self.assertIsNone(meymayakkam_4("சிறப்பு"))
        self.assertIsNone(meymayakkam_4("வர்ணம்"))

#    def test_meymayakkam_(self):
#        # Correct Words
#        self.assertTrue(meymayakkam_(""))
#        self.assertTrue(meymayakkam_(""))
#        self.assertTrue(meymayakkam_(""))
#        self.assertTrue(meymayakkam_(""))
#        # Detecting Incorrect words
#        self.assertFalse(meymayakkam_(""))
#        self.assertFalse(meymayakkam_(""))
#        # Detecting Not Applicable words
#        self.assertIsNone(meymayakkam_(""))
#        self.assertIsNone(meymayakkam_(""))
#        self.assertIsNone(meymayakkam_(""))

